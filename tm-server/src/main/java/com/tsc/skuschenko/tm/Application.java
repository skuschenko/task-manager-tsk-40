package com.tsc.skuschenko.tm;

import com.tsc.skuschenko.tm.bootstrap.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}