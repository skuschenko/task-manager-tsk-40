package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyDescriptionException;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.Task;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public final class TaskService implements ITaskService {

    @NotNull
    private final IConnectionService connectionService;

    public TaskService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(
            @Nullable final String userId, @Nullable final String name,
            @Nullable final String description
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(description)
                .orElseThrow(EmptyDescriptionException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        @NotNull final ITaskRepository projectRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            projectRepository.add(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@Nullable final List<Task> tasks) {
        tasks.forEach(item ->
                add(item.getUserId(), item.getName(), item.getDescription())
        );
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status.getDisplayName());
            task.setStatus(status.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status.getDisplayName());
            task.setStatus(status.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task changeStatusByName(
            @NotNull final String userId, @Nullable final String name,
            @Nullable final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(TaskNotFoundException::new);
            task.setStatus(status.getDisplayName());
            task.setStatus(status.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;

        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            entityRepository.clear(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        try {
            @NotNull final ITaskRepository taskRepository =
                    sqlSession.getMapper(ITaskRepository.class);
            taskRepository.clearAllTasks();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task completeById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(TaskNotFoundException::new);
            task.setDateFinish(new Date());
            task.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task completeByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(TaskNotFoundException::new);
            task.setDateFinish(new Date());
            task.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;

        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task completeByName(@NotNull final String userId,
                               @Nullable final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(TaskNotFoundException::new);
            task.setDateFinish(new Date());
            task.setStatus(Status.COMPLETE.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(
            @NotNull final String userId,
            @Nullable final Comparator<Task> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {

            return entityRepository.findAllWithUserId(userId)
                    .stream()
                    .sorted(comparator)
                    .collect(Collectors.toList());
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List findAll(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            return entityRepository.findAllWithUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            return entityRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            return entityRepository.findOneById(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            return entityRepository.findOneByIndex(userId, index);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByName(
            @NotNull final String userId, final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            return entityRepository.findOneByName(userId, name);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneById(
            @NotNull final String userId, final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneById(userId, id))
                            .orElseThrow(TaskNotFoundException::new);
            entityRepository.removeTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneByIndex(
            @NotNull final String userId, final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneByIndex(userId, index))
                            .orElseThrow(TaskNotFoundException::new);
            entityRepository.removeTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task removeOneByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task =
                    Optional.ofNullable(findOneByName(userId, name))
                            .orElseThrow(TaskNotFoundException::new);
            entityRepository.removeTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startById(
            @NotNull final String userId, @Nullable final String id
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        @NotNull final Task task = Optional.ofNullable(
                findOneById(userId, id)
        ).orElseThrow(TaskNotFoundException::new);
        task.setDateFinish(new Date());
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityRepository.updateTaskQuery(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByIndex(
            @NotNull final String userId, @Nullable final Integer index
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(TaskNotFoundException::new);
            task.setDateFinish(new Date());
            task.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task startByName(
            @NotNull final String userId, @Nullable final String name
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(
                    findOneByName(userId, name)
            ).orElseThrow(TaskNotFoundException::new);
            task.setDateFinish(new Date());
            task.setStatus(Status.IN_PROGRESS.getDisplayName());
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateOneById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(
                    findOneById(userId, id)
            ).orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task updateOneByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        @NotNull final SqlSession sqlSession =
                connectionService.getSqlConnection();
        @NotNull final ITaskRepository entityRepository =
                sqlSession.getMapper(ITaskRepository.class);
        try {
            @NotNull final Task task = Optional.ofNullable(
                    findOneByIndex(userId, index)
            ).orElseThrow(TaskNotFoundException::new);
            task.setName(name);
            task.setDescription(description);
            entityRepository.updateTaskQuery(task);
            sqlSession.commit();
            return task;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}