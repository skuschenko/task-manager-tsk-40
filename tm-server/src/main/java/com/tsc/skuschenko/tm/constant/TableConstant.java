package com.tsc.skuschenko.tm.constant;

public interface TableConstant {

    String PROJECT_TABLE = "tm_project";

    String SESSION_TABLE = "tm_session";

    String TASK_TABLE = "tm_task";

    String USER_TABLE = "tm_user";

}
