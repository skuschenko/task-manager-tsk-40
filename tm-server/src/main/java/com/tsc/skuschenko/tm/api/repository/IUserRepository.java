package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.User;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository {
    @Insert("INSERT INTO tm_user" +
            "(id, email, firstName, lastName, locked, login, middleName, " +
            "passwordHash,role) VALUES(#{id},#{email},#{firstName}," +
            "#{lastName},#{isLocked},#{login},#{middleName},#{passwordHash}," +
            "#{role})")
    void add(@NotNull User user);

    @Delete("DELETE * FROM tm_user")
    void clear();

    @Select("SELECT * FROM tm_user")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "firstName"),
            @Result(property = "isLocked", column = "locked"),
            @Result(property = "login", column = "login"),
            @Result(property = "middleName", column = "middleName"),
            @Result(property = "passwordHash", column = "passwordHash"),
            @Result(property = "role", column = "role"),
    })
    @Nullable List<User> findAll();

    @Select("SELECT * FROM tm_user WHERE email = #{email} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "firstName"),
            @Result(property = "isLocked", column = "locked"),
            @Result(property = "login", column = "login"),
            @Result(property = "middleName", column = "middleName"),
            @Result(property = "passwordHash", column = "passwordHash"),
            @Result(property = "role", column = "role"),
    })
    @Nullable
    User findByEmail(@NotNull String email);

    @Select("SELECT * FROM tm_user WHERE id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "firstName"),
            @Result(property = "isLocked", column = "locked"),
            @Result(property = "login", column = "login"),
            @Result(property = "middleName", column = "middleName"),
            @Result(property = "passwordHash", column = "passwordHash"),
            @Result(property = "role", column = "role"),
    })
    @Nullable
    User findById(@NotNull String id);

    @Select("SELECT * FROM tm_user WHERE login = #{login} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "email", column = "email"),
            @Result(property = "firstName", column = "firstName"),
            @Result(property = "isLocked", column = "locked"),
            @Result(property = "login", column = "login"),
            @Result(property = "middleName", column = "middleName"),
            @Result(property = "passwordHash", column = "passwordHash"),
            @Result(property = "role", column = "role"),
    })
    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    @Delete("DELETE FROM tm_user WHERE login = #{login}")
    User removeByLogin(@NotNull String login);

    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeOneById(@NotNull String id);

    @Update("UPDATE tm_user SET id=#{id}, email=#{email}, " +
            "firstName=#{firstName}, lastName=#{lastName}, locked=#{isLocked}," +
            "login=#{login}, middleName=#{middleName}, " +
            "passwordHash=#{passwordHash},role=#{role}")
    void updateUserQuery(@NotNull User user);

}
