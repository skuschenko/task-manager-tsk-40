package com.tsc.skuschenko.tm.service;


import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ISessionRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.exception.system.ConnectionFailedException;
import lombok.SneakyThrows;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @SneakyThrows
    @Override
    public Connection getConnection() {
        @NotNull final Map<String, String> properties = getConnectionProperty();
        @NotNull Connection connection =
                DriverManager.getConnection(
                        properties.get("url"),
                        properties.get("userName"),
                        properties.get("password")
                );
        connection.setAutoCommit(false);
        return connection;
    }

    @NotNull
    private Map<String, String> getConnectionProperty() {
        @NotNull final Map<String, String> properties =
                new HashMap<String, String>();
        @Nullable final String driver = propertyService.getJdbcDriver();
        Optional.ofNullable(driver).orElseThrow(() ->
                new ConnectionFailedException("Driver")
        );
        @Nullable final String userName = propertyService.getJdbcUserName();
        Optional.ofNullable(userName).orElseThrow(() ->
                new ConnectionFailedException("UserName")
        );
        @Nullable final String password = propertyService.getJdbcPassword();
        Optional.ofNullable(password).orElseThrow(() ->
                new ConnectionFailedException("Password")
        );
        @Nullable final String url = propertyService.getJdbcUrl();
        Optional.ofNullable(url).orElseThrow(() ->
                new ConnectionFailedException("Url")
        );
        properties.put("driver", driver);
        properties.put("userName", userName);
        properties.put("password", password);
        properties.put("url", url);
        return properties;
    }

    @Override
    @NotNull
    public SqlSession getSqlConnection() {
        return sqlSessionFactory.openSession();
    }

    @Override
    @NotNull
    public SqlSessionFactory getSqlSessionFactory() {
        @NotNull final Map<String, String> properties = getConnectionProperty();
        @NotNull final DataSource dataSource = new PooledDataSource(
                properties.get("driver"),
                properties.get("url"),
                properties.get("userName"),
                properties.get("password")
        );
        @NotNull final TransactionFactory transactionFactory =
                new JdbcTransactionFactory();
        @NotNull final Environment environment =
                new Environment("development", transactionFactory, dataSource);
        @NotNull final Configuration configuration =
                new Configuration(environment);
        configuration.addMapper(IUserRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}
