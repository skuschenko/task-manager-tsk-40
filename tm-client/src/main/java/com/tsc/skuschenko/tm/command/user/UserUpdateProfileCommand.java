package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "update profile of current user";

    @NotNull
    private static final String NAME = "update-user-profile";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @NotNull final String userId
                = session.getUserId();
        showParameterInfo("first name");
        @NotNull final String firstName = TerminalUtil.nextLine();
        showParameterInfo("last name");
        @NotNull final String lastName = TerminalUtil.nextLine();
        showParameterInfo("middle name");
        @NotNull final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
