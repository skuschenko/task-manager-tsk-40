package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Optional;

public final class TaskFindAllByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "find all task by project id";

    @NotNull
    private static final String NAME = "find-all-task-by-project-id";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("project id");
        @NotNull final String value = TerminalUtil.nextLine();
        @Nullable final List<Task> tasks
                = serviceLocator.getTaskEndpoint()
                .findAllTaskByProjectId(session, value);
        Optional.ofNullable(tasks).filter(item -> item.size() != 0)
                .ifPresent(item -> item
                        .forEach(this::showTask));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
