package com.tsc.skuschenko.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-03-18T23:29:14.181+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "UserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface UserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/createUserRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/createUserResponse")
    @RequestWrapper(localName = "createUser", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CreateUser")
    @ResponseWrapper(localName = "createUserResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CreateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User createUser(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/createUserWithEmailRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/createUserWithEmailResponse")
    @RequestWrapper(localName = "createUserWithEmail", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CreateUserWithEmail")
    @ResponseWrapper(localName = "createUserWithEmailResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CreateUserWithEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User createUserWithEmail(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "email", targetNamespace = "")
                    java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/createUserWithRoleRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/createUserWithRoleResponse")
    @RequestWrapper(localName = "createUserWithRole", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CreateUserWithRole")
    @ResponseWrapper(localName = "createUserWithRoleResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CreateUserWithRoleResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User createUserWithRole(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password,
            @WebParam(name = "role", targetNamespace = "")
                    java.lang.String role
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/findUserByEmailRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/findUserByEmailResponse")
    @RequestWrapper(localName = "findUserByEmail", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.FindUserByEmail")
    @ResponseWrapper(localName = "findUserByEmailResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.FindUserByEmailResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User findUserByEmail(
            @WebParam(name = "email", targetNamespace = "")
                    java.lang.String email
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/findUserByLoginRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/findUserByLoginResponse")
    @RequestWrapper(localName = "findUserByLogin", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.FindUserByLogin")
    @ResponseWrapper(localName = "findUserByLoginResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.FindUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User findUserByLogin(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/getUserRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/getUserResponse")
    @RequestWrapper(localName = "getUser", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.GetUser")
    @ResponseWrapper(localName = "getUserResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.GetUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User getUser();

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/isEmailExistRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/isEmailExistResponse")
    @RequestWrapper(localName = "isEmailExist", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.IsEmailExist")
    @ResponseWrapper(localName = "isEmailExistResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.IsEmailExistResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean isEmailExist(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/isLoginExistRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/isLoginExistResponse")
    @RequestWrapper(localName = "isLoginExist", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.IsLoginExist")
    @ResponseWrapper(localName = "isLoginExistResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.IsLoginExistResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean isLoginExist(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/lockUserByLoginRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/lockUserByLoginResponse")
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.LockUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User lockUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    com.tsc.skuschenko.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/removeUserByLoginRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/removeUserByLoginResponse")
    @RequestWrapper(localName = "removeUserByLogin", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.RemoveUserByLogin")
    @ResponseWrapper(localName = "removeUserByLoginResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.RemoveUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User removeUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    com.tsc.skuschenko.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/setPasswordRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/setPasswordResponse")
    @RequestWrapper(localName = "setPassword", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.SetPassword")
    @ResponseWrapper(localName = "setPasswordResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.SetPasswordResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User setPassword(
            @WebParam(name = "userId", targetNamespace = "")
                    java.lang.String userId,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/unlockUserByLoginRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/unlockUserByLoginResponse")
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.UnlockUserByLoginResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User unlockUserByLogin(
            @WebParam(name = "session", targetNamespace = "")
                    com.tsc.skuschenko.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/updateUserRequest", output = "http://endpoint.tm.skuschenko.tsc.com/UserEndpoint/updateUserResponse")
    @RequestWrapper(localName = "updateUser", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.UpdateUser")
    @ResponseWrapper(localName = "updateUserResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.UpdateUserResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.User updateUser(
            @WebParam(name = "userId", targetNamespace = "")
                    java.lang.String userId,
            @WebParam(name = "firstName", targetNamespace = "")
                    java.lang.String firstName,
            @WebParam(name = "lastName", targetNamespace = "")
                    java.lang.String lastName,
            @WebParam(name = "middleName", targetNamespace = "")
                    java.lang.String middleName
    );
}
